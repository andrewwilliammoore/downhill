const path = require('path');

const assetsConfig = require('./src/config').assets;
const babelConfig = require('./.babelrc');

module.exports = {
  entry: path.resolve(__dirname, 'src/entry.js'),
  output: {
    path: path.resolve(__dirname, assetsConfig.basePath)
  },
  mode: process.env.WEBPACK_BUILD_MODE || 'production',
  watch: process.env.WEBPACK_BUILD_MODE === 'development',
  watchOptions: { poll: true },
  module: {
    rules: [
      {
        test: /.js$/,
        use: {
          loader: 'babel-loader',
          options: babelConfig
        }
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      }
    ]
  }
};
