import './style.css';

import { registerHandlers, sizeBoard, drawBoard } from './app/gameboard';

registerHandlers();
sizeBoard();
drawBoard();
