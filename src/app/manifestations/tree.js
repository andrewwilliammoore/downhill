import * as player from './player';
import { drawShape } from '../rendering';
import { baseOffsetY, estimatedViewportWidth, slopeHeight, numberOfScreensPerRun } from '../coordinates';

const width = 75;
const height = 75;
const color = 'green';

const treeCount = 5 * numberOfScreensPerRun;
const treeLine = baseOffsetY * 2 + height;

const getRandomArbitrary = (min, max) => (
  Math.random() * (max - min) + min
);

const entities = {};
for (let i = 0; i < treeCount; i++) {
  const x = Math.floor(getRandomArbitrary(0, estimatedViewportWidth));
  const y = Math.floor(getRandomArbitrary(treeLine, slopeHeight));

  if (typeof entities[y] === 'undefined') {
    entities[y] = {};
  }
  entities[y][x] = { x };
}

const getEntitiesForViewport = (playerPositionY, boardHeight) => {
  const viewportTopPosition = playerPositionY - baseOffsetY;
  const viewportBottomPosition = viewportTopPosition + boardHeight;

  const entitiesForViewport = [];
  for (let y = viewportTopPosition; y < viewportBottomPosition; y++) {
    if (!entities[y]) { continue; }
    Object.values(entities[y]).forEach(({ x }) => {
      entitiesForViewport.push({ x, y });
    });
  }

  return entitiesForViewport;
};

const getViewportPosition = (position, playerPositionY) => {
  return {
    x: position.x,
    y: position.y - playerPositionY
  };
};

const generateDrawCoordinates = (x, y) => ([
  [x, y],
  [x + Math.floor(width / 2), y + height],
  [x - Math.floor(width / 2), y + height]
]);

// Begin exports

export const draw = (context, boardHeight) => {
  const playerPositionY = player.getPosition().y;

  getEntitiesForViewport(playerPositionY, boardHeight).forEach((position) => {
    const { x, y } = getViewportPosition(position, playerPositionY);
    if (y < height * -1) { return; }
    const coordinates = generateDrawCoordinates(x, y);
    const orientation = 0.0;

    drawShape(context, color, x, y, orientation, coordinates);
  });
};
