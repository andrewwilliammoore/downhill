import store from '../store';
import { updateResults } from '../resources/race';
import { baseOffsetX, baseOffsetY, slopeHeight } from '../coordinates';
import { drawShape } from '../rendering';

const width = 50;
const height = 50;
const color = 'blue';
const pixelIncrementPerInterval = 2;

const position = {
  x: baseOffsetX,
  y: baseOffsetY
};

const getViewportPosition = () => {
  return {
    x: position.x,
    y: baseOffsetY
  };
};

const generateDrawCoordinates = (x, y) => ([
  [x - Math.floor(width / 2), y],
  [x + Math.floor(width / 2), y],
  [x, y + height]
]);

// Begin exports

export const getPosition = () => (Object.assign({}, position));

export const draw = (context, boardHeight) => {
  const { x, y } = getViewportPosition();
  const coordinates = generateDrawCoordinates(x, y);
  const { orientation } = store.getState().player;

  drawShape(context, color, x, y, orientation, coordinates, boardHeight);
};

export const setPosition = () => {
  const { orientation } = store.getState().player;
  const orientationRatio = Math.abs(orientation) / 90.0;
  const candidateX = orientation > 0 ? position.x - orientationRatio : position.x + orientationRatio;

  position.y += pixelIncrementPerInterval;
  position.x = candidateX <= 1 ? 1 : (candidateX < window.innerWidth ? candidateX : window.innerWidth - 1);

  if (position.y >= slopeHeight) {
    store.dispatch(updateResults());
  }
};
