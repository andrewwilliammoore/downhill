import { handleResize, handleLetGo } from './handlers';
import * as player from './manifestations/player';
import * as tree from './manifestations/tree';

const element = document.querySelector('#gameboard');

// Begin exports

export const registerHandlers = () => {
  window.onresize = handleResize();
  element.addEventListener('mouseup', handleLetGo);
};

export const sizeBoard = () => {
  element.width = window.innerWidth;
  element.height = window.innerHeight;
};

export const drawBoard = () => {
  const context = element.getContext('2d');

  context.clearRect(0, 0, element.width, element.height);

  player.draw(context, element.height);
  tree.draw(context, element.height);
};
