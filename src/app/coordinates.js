export const baseOffsetX = Math.floor(window.innerWidth / 2);
export const baseOffsetY = Math.floor(window.innerHeight / 5.5);

export const estimatedViewportHeight = 700;
export const estimatedViewportWidth = 1920;

export const numberOfScreensPerRun = 20;
export const slopeHeight = estimatedViewportHeight * numberOfScreensPerRun;
