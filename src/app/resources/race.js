import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  startTime: new Date().toString(),
  endTime: null
};

const race = createSlice({
  name: 'race',
  initialState,
  reducers: {
    updateResults (state) {
      state.endTime = new Date().toString();
    }
  }
});

export const { updateResults } = race.actions;

export default race.reducer;
