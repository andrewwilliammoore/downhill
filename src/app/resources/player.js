import { createSlice } from '@reduxjs/toolkit';
import { estimatedViewportHeight } from '../coordinates';

const initialState = {
  speed: 0,
  orientation: 0.0
};

const maxOrientation = 90;
const minOrientation = maxOrientation * -1;
const maxSpeed = estimatedViewportHeight * 1.3; // pixels per second

// Restrict orientation to allowed values
const getBoundedOrientation = (orientation) => {
  if (orientation > maxOrientation) {
    return maxOrientation;
  } else if (orientation < minOrientation) {
    return minOrientation;
  }

  return orientation;
};

const player = createSlice({
  name: 'player',
  initialState,
  reducers: {
    updateOrientation (state, action) {
      const { orientation } = action.payload;

      state.orientation = getBoundedOrientation(orientation);
    },
    updateSpeed (state, action) {
      const { surfaceResistance, eventDistanceY } = action.payload;
      const resistance = surfaceResistance + Math.abs(state.orientation) / 90.0;
      const speed = Math.floor(state.speed + eventDistanceY - resistance);

      state.speed = speed >= maxSpeed ? maxSpeed : speed <= 0 ? 0 : speed;
    }
  }
});

export const { updateOrientation, updateSpeed } = player.actions;

export default player.reducer;
