import { setPosition } from './manifestations/player';
import { drawBoard } from './gameboard';

const intervals = {
  playerMotion: null
};

const animations = {
  player: null
};

const startUpdateInterval = (speed) => {
  const maximumDelay = 50;
  const minimumDelay = 1;
  const candidateDelay = maximumDelay - speed / 1000 * maximumDelay;
  const delay = candidateDelay > minimumDelay ? candidateDelay : minimumDelay;

  if (intervals.playerMotion !== null) { clearInterval(intervals.playerMotion); };
  intervals.playerMotion = setInterval(setPosition, Math.floor(delay));
};

const animation = () => {
  drawBoard();
  startAnimation();
};

const startAnimation = () => {
  if (animations.player !== null) { cancelAnimationFrame(animations.player); };
  animations.player = requestAnimationFrame(animation);
};

const showResults = (startTime, endTime) => {
  const result = new Date(endTime) - new Date(startTime);
  const seconds = (result / 1000).toFixed(2);
  alert(`Race complete! Your time: ${seconds} seconds`);
};

// Begin exports

export const go = (speed) => {
  startUpdateInterval(speed);
  startAnimation();
};

export const stop = () => {
  clearInterval(intervals.playerMotion);
  cancelAnimationFrame(animations.player);
  animations.player = null;
};

export const endRace = (startTime, endTime) => {
  stop();
  showResults(startTime, endTime);
};
