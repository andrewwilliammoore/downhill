const rotate = (cx, cy, x, y, boardHeight, angle) => {
  const cartesianCY = boardHeight - cy;
  const cartesianY = boardHeight - y;
  const radians = (Math.PI / 180) * angle;
  const cos = Math.cos(radians);
  const sin = Math.sin(radians);
  const nx = (cos * (x - cx)) + (sin * (cartesianY - cartesianCY)) + cx;
  const cartesianNy = (cos * (cartesianY - cartesianCY)) - (sin * (x - cx)) + cartesianCY;
  const ny = boardHeight - cartesianNy;

  return [nx, ny];
};

// Begin exports

export const drawShape = (context, fillStyle, positionX, positionY, orientation, coordinates, boardHeight) => {
  context.fillStyle = fillStyle;
  context.beginPath();
  coordinates.forEach((candidates, index) => {
    const func = index === 0 ? 'moveTo' : 'lineTo';

    let coordinatesToDraw;
    if (orientation !== 0 && boardHeight) {
      coordinatesToDraw = rotate(positionX, positionY, candidates[0], candidates[1], boardHeight, orientation);
    } else {
      coordinatesToDraw = candidates;
    }

    context[func](coordinatesToDraw[0], coordinatesToDraw[1]);
  });
  context.fill();
};
