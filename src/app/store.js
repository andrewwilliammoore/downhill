import { configureStore } from '@reduxjs/toolkit';
import playerReducer from './resources/player';
import raceReducer from './resources/race';
import { go, stop, endRace } from './window';

const store = configureStore({
  reducer: {
    player: playerReducer,
    race: raceReducer
  }
});

store.subscribe(() => {
  const { player, race } = store.getState();
  const { speed } = player;
  const { startTime, endTime } = race;

  if (endTime) return endRace(startTime, endTime);

  speed <= 0 ? stop() : go(speed);
});

export default store;
