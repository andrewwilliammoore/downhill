import { updateOrientation, updateSpeed } from './resources/player';
import { getPosition } from './manifestations/player';
import { baseOffsetY } from './coordinates';
import { sizeBoard, drawBoard } from './gameboard';
import store from './store';

// Begin exports

export const handleLetGo = (event) => {
  event.stopPropagation();
  const eventDistanceX = event.offsetX - getPosition().x;
  const eventDistanceY = event.offsetY - baseOffsetY;
  const surfaceResistance = 0.5; // TODO: vary depending on terrain, etc
  // Player facing straight downhill is considered 0 degrees. Screen left is 90, screen right is -90.
  const orientation = Math.atan2(eventDistanceY, eventDistanceX) * 180 / Math.PI - 90;

  store.dispatch(updateOrientation({ orientation }));
  store.dispatch(updateSpeed({ surfaceResistance, eventDistanceY }));
};

export const handleResize = () => (event) => {
  sizeBoard();
  drawBoard();
};
